package com.example.ajiguna.asmart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ardi on 18/04/18.
 */

public class RelayModel {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("motorpump")
    @Expose
    private Boolean motorpump;
    @SerializedName("solenoid")
    @Expose
    private Boolean solenoid;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getMotorpump() {
        return motorpump;
    }

    public void setMotorpump(Boolean motorpump) {
        this.motorpump = motorpump;
    }

    public Boolean getSolenoid() {
        return solenoid;
    }

    public void setSolenoid(Boolean solenoid) {
        this.solenoid = solenoid;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }
}
