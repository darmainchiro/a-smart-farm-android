package com.example.ajiguna.asmart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aji Guna on 08/05/2018.
 */

public class Condition {

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("sayuran")
    @Expose
    private String sayuran;
    @SerializedName("temperature")
    @Expose
    private Double temperature;
    @SerializedName("humidity")
    @Expose
    private Double humidity;
    @SerializedName("soilmoisture")
    @Expose
    private Double soilmoisture;
    @SerializedName("airpressure")
    @Expose
    private Double airpressure;
    @SerializedName("batteray")
    @Expose
    private Double batteray;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("request")
    @Expose
    private Request request;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSayuran() {
        return sayuran;
    }

    public void setSayuran(String sayuran) {
        this.sayuran = sayuran;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getSoilmoisture() {
        return soilmoisture;
    }

    public void setSoilmoisture(Double soilmoisture) {
        this.soilmoisture = soilmoisture;
    }

    public Double getAirpressure() {
        return airpressure;
    }

    public void setAirpressure(Double airpressure) {
        this.airpressure = airpressure;
    }

    public Double getBatteray() {
        return batteray;
    }

    public void setBatteray(Double batteray) {
        this.batteray = batteray;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
