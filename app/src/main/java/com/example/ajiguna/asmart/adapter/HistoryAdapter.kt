package com.example.ajiguna.asmart.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.ajiguna.asmart.MainActivity
import com.example.ajiguna.asmart.R
import com.example.ajiguna.asmart.model.Condition
import java.util.*


class HistoryAdapter(
        internal var context: Context) : RecyclerView.Adapter<HistoryAdapter.MyViewHolder>() {

        internal val features: MutableList<Condition>?
        internal var isLoadingAdded: Boolean? = false
        internal var retryPageLoad: Boolean? = false
        internal var errorMsg: String? = null


    init {
        features = ArrayList<Condition>()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // infalte the item Layout
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_dashboard, parent, false)
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // set the data in items
        val holderku = holder
        holderku.name.text = features!!.get(position).temperature.toString()
        // implement setOnClickListener event on item view.
        holder.nView.setOnClickListener { view ->
            // display a toast with person name on item click
            val context = view.context
            val intent: Intent
            when (position) {
                0 ->
                     intent = Intent(context, MainActivity::class.java)

                1 -> intent = Intent(context, MainActivity::class.java)

                2 -> intent = Intent(context, MainActivity::class.java)

                else -> intent = Intent(context, MainActivity::class.java)
            }
            context.startActivity(intent)
        }

    }


    override fun getItemCount(): Int {
        return features?.size ?: 0
    }


    inner class MyViewHolder(
            // init the item view's
        internal var nView: View) : RecyclerView.ViewHolder(nView), View.OnClickListener {
        internal var name: TextView
        internal var image: ImageView

        init {
            // get the reference of item view's
            name = nView.findViewById<View>(R.id.parameter) as TextView
            image = nView.findViewById<View>(R.id.image_parameter) as ImageView
        }
        override fun onClick(v: View) {
        }
    }

    /*
        Helpers - Pagination
   _________________________________________________________________________________________________
    */

    fun add(r: Condition) {
        features!!.add(r)
        notifyItemInserted(features!!.size - 1)
    }

    fun addAll(moveResults: List<Condition>) {
        for (result in moveResults) {
            add(result)
        }
    }

    fun remove(r: Condition) {
        val position = features!!.indexOf(r)
        if (position > -1) {
            features.removeAt(position)
            notifyItemRemoved(position)
        }
    }


    fun isEmpty(): Boolean {
        return itemCount == 0
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Condition())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = features!!.size - 1
        val result = getItem(position)

        if (result != null) {
            features!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): Condition? {
        return features!!.get(position)
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(features!!.size - 1)

        if (errorMsg != null) this.errorMsg = errorMsg
    }

}
