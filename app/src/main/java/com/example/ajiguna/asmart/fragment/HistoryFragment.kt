package com.example.ajiguna.asmart.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ajiguna.asmart.R
import com.example.ajiguna.asmart.adapter.HistoryAdapter
import com.example.ajiguna.asmart.api.ApiClient
import com.example.ajiguna.asmart.api.ApiInterface
import com.example.ajiguna.asmart.model.Condition
import com.example.ajiguna.asmart.model.LatestAll
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



/**
 * Created by Aji Guna on 01/03/2018.
 */

class HistoryFragment : Fragment() {
    internal var toolbar: Toolbar? = null
    internal lateinit var call_condition: Call<LatestAll>
    internal var apiInterface: ApiInterface? = null
    internal var historyAdapter: HistoryAdapter? = null

    internal var recy_history: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_history, container, false)

        recy_history = view.findViewById(R.id.recy_history) as RecyclerView

        historyAdapter = HistoryAdapter(activity)

        val linearLayoutManager = LinearLayoutManager(activity)
        recy_history!!.setLayoutManager(linearLayoutManager)
        recy_history!!.setItemAnimator(DefaultItemAnimator())
        recy_history!!.setAdapter(historyAdapter)

        viewHistory()
        return view
    }

    private fun viewHistory() {
        apiInterface = ApiClient.getClient().create(ApiInterface::class.java)
        call_condition = apiInterface!!.conditons

        call_condition.enqueue(object : Callback<LatestAll> {
            override fun onResponse(call_event: Call<LatestAll>, response: Response<LatestAll>) {

                val features = fetchResults(response)
                //                adapter = new Button_Adapter(ContenActivity.this, post_topic);
                //                recyclerView.setAdapter(adapter);
                historyAdapter!!.addAll(features)
            }

            override fun onFailure(call_event: Call<LatestAll>, t: Throwable) {
                t.printStackTrace()
            }

        })
    }

    private fun fetchResults(response: Response<LatestAll>): List<Condition> {
        val eventku = response.body()
        return eventku!!.conditions
    }

    companion object {
        fun newInstance(): HistoryFragment {
            return HistoryFragment()
        }
    }
}
