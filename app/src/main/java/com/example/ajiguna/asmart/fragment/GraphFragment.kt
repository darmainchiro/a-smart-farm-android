package com.example.ajiguna.asmart.fragment

import android.R.attr.action
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.db.chart.model.LineSet
import com.db.chart.renderer.AxisRenderer
import com.db.chart.tooltip.Tooltip
import com.db.chart.util.Tools
import com.db.chart.view.LineChartView
import com.example.ajiguna.asmart.R




/**
 * Created by Aji Guna on 01/03/2018.
 */
class GraphFragment : Fragment() {
    internal var toolbar: Toolbar? = null
    internal var chartTemperature: LineChartView? = null

    private val mLabels = arrayOf("Jan", "Fev", "Mar", "Apr", "Jun", "May", "Jul", "Aug", "Sep")
    private val mValue = arrayOf(floatArrayOf(3.5f, 4.7f, 4.3f, 8f, 6.5f, 9.9f, 7f, 8.3f, 7.0f), floatArrayOf(4.5f, 2.5f, 2.5f, 9f, 4.5f, 9.5f, 5f, 8.3f, 1.8f))

    private val mBaseAction: Runnable? = null
    private val mTip: Tooltip? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_graph, container, false)
        chartTemperature = view!!.findViewById(R.id.chart_temperature) as LineChartView

        val dataset = LineSet(mLabels, mValue[0])
        dataset.setColor(Color.parseColor("#b3b5bb"))
                .setFill(Color.parseColor("#2d374c"))
                .setDotsColor(Color.parseColor("#ffc755"))
                .setThickness(4f)
                .endAt(6);

        chartTemperature!!.addData(dataset);

        val mBaseAction = action
        val chartAction = Runnable {
            mBaseAction.run()
            mTip!!.prepare(chartTemperature!!.getEntriesArea(0).get(3), mValue[0][3])
            chartTemperature!!.showTooltip(mTip, true)
        }
        val thresPaint = Paint()
        thresPaint.setColor(Color.parseColor("#0079AE"))
        thresPaint.setStyle(Paint.Style.STROKE)
        thresPaint.setAntiAlias(true)
        thresPaint.setStrokeWidth(Tools.fromDpToPx(.75f))
        thresPaint.setPathEffect(DashPathEffect(floatArrayOf(10f, 10f), 0f))

        chartTemperature!!.setAxisBorderValues(0f, 20f)
                .setYLabels(AxisRenderer.LabelPosition.NONE)
                .setTooltips(mTip)
                .show()

        return view
    }

    companion object {
        fun newInstance(): GraphFragment {
            return GraphFragment()
        }
    }
}

private fun Any.run() {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

}


