package com.example.ajiguna.asmart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.ToggleButton;

import com.example.ajiguna.asmart.api.ApiInterface;
import com.example.ajiguna.asmart.config.Aplikasi;
import com.example.ajiguna.asmart.model.RelayModel;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SocketActivity extends AppCompatActivity {

    private Switch sw_water;
    private Switch sw_solenoid;

    private ToggleButton btn_motor;
    private ToggleButton btn_solenoid;
    private Button motorku;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(Aplikasi.URL_HOST);
        }catch (URISyntaxException e){

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relay);

        mSocket.on("statuswater", onStatusWater);
        mSocket.on("relay1", onStatusMotor);

//        sw_water = (Switch) findViewById(R.id.motor_pump);
//        sw_solenoid = (Switch) findViewById(R.id.solenoid);


        Retrofit retro = new Retrofit.Builder().baseUrl(Aplikasi.URL_HOST)
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiInterface service = retro.create(ApiInterface.class);
        Call<RelayModel> call = service.getRelay();

        call.enqueue(new Callback<RelayModel>() {
            @Override
            public void onResponse(Call<RelayModel> call, Response<RelayModel> response) {
                if(response.isSuccessful()){
                    btn_motor = (ToggleButton) findViewById(R.id.btn_motor);
                    if(response.body().getMotorpump()){
                        btn_motor.setText("On");
                    }else{
                        btn_motor.setText("Off");
                    }

                    btn_solenoid = (ToggleButton) findViewById(R.id.btn_solenoid);
                    if(response.body().getSolenoid()){
                        btn_solenoid.setText("On");
                    }else{
                        btn_solenoid.setText("Off");
                    }
                }
            }

            @Override
            public void onFailure(Call<RelayModel> call, Throwable t) {

            }
        });

        btn_motor = (ToggleButton) findViewById(R.id.btn_motor);
        btn_motor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    JSONObject obj1 = new JSONObject();
                    try {
                        obj1.put("msg", true);
                    }catch (JSONException e) {
                        return;
                    }
                    btn_motor.setText("On");
                    mSocket.emit("relay1", obj1);
                }
                else{
                    JSONObject obj1 = new JSONObject();
                    try {
                        obj1.put("msg", false);
                    }catch (JSONException e) {
                        return;
                    }
                    btn_motor.setText("Off");
                    mSocket.emit("relay1", obj1);
                }
            }
        });

        btn_solenoid = (ToggleButton) findViewById(R.id.btn_solenoid);
        btn_solenoid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    JSONObject obj1 = new JSONObject();
                    try {
                        obj1.put("msg", true);
                    }catch (JSONException e) {
                        return;
                    }
                    btn_solenoid.setText("On");
                    mSocket.emit("statuswater", obj1);
                }
                else{
                    JSONObject obj1 = new JSONObject();
                    try {
                        obj1.put("msg", false);
                    }catch (JSONException e) {
                        return;
                    }
                    btn_solenoid.setText("Off");
                    mSocket.emit("statuswater", obj1);
                }
            }
        });

        motorku = (Button) findViewById(R.id.motorku);
        motorku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject obj1 = new JSONObject();
                try {
                    obj1.put("msg", true);
                }catch (JSONException e) {
                    return;
                }
                btn_motor.setText("On");
                mSocket.emit("relay1", obj1);
            }
        });

    }

    private Emitter.Listener onStatusWater  = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String msg;
                    try {
                        msg = data.getString("msg");
                    } catch (JSONException e) {
                        return;
                    }
                    Log.e("status lampu", msg);
                    btn_solenoid = (ToggleButton) findViewById(R.id.btn_solenoid);
                    if(msg == "true"){
//                        imgAppsRespon.setImageResource(R.drawable.kondisireadyoff);
                        btn_solenoid.setChecked(false);
                    }
                    else{
//                        imgAppsRespon.setImageResource(R.drawable.kondisireadyon);
                        btn_solenoid.setChecked(true);
                    }
                }
            });
        }
    };

    private Emitter.Listener onStatusMotor  = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String msg;
                    try {
                        msg = data.getString("msg");
                    } catch (JSONException e) {
                        return;
                    }
                    Log.e("status lampu", msg);
                    btn_motor = (ToggleButton) findViewById(R.id.btn_motor);
                    if(msg == "true"){
//                        imgAppsRespon.setImageResource(R.drawable.kondisireadyoff);
                        btn_motor.setChecked(false);
                    }
                    else{
//                        imgAppsRespon.setImageResource(R.drawable.kondisireadyon);
                        btn_motor.setChecked(true);
                    }
                }
            });
        }
    };
}
