package com.example.ajiguna.asmart

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

/**
 * Created by Aji Guna on 21/03/2018.
 */

class SettingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        var mToolbar = findViewById(R.id.toolbar_tabs) as Toolbar?
        mToolbar!!.setTitle("Pengaturan")
    }
}
