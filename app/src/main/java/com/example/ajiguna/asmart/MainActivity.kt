package com.example.ajiguna.asmart

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.example.ajiguna.asmart.fragment.GraphFragment
import com.example.ajiguna.asmart.fragment.HistoryFragment
import com.example.ajiguna.asmart.fragment.HomeFragment

class MainActivity : AppCompatActivity() {

    private val mTextMessage: TextView? = null
    private var toolbar: Toolbar? = null
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        var selectedFragment: Fragment? = null
      when (item.itemId) {
            R.id.navigation_home -> selectedFragment = HomeFragment.newInstance()
            R.id.navigation_graph -> selectedFragment = GraphFragment.newInstance()
            R.id.navigation_history -> selectedFragment = HistoryFragment.newInstance()
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment)
        transaction.commit()
       true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var mToolbar = findViewById(R.id.toolbar_tabs) as Toolbar?
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        val navigation = findViewById<View>(R.id.navigation) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        //Manually displaying the first fragment - one time only
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance())
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.getItemId()
        if (id == R.id.menu_setting) {
            startActivity(Intent(this, SettingActivity::class.java))
        }else if (id == R.id.menu_info) {
            startActivity(Intent(this, InfoActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
