package com.example.ajiguna.asmart.api;


import com.example.ajiguna.asmart.model.LatestAll;
import com.example.ajiguna.asmart.model.RelayModel;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Aji Guna on 28/07/2017.
 */

public interface ApiInterface {

//    @FormUrlEncoded
//    @POST("login.php")
//    Call<Login> getLogin(
//            @FieldMap HashMap<String, String> params
//    );
//
    @GET("conditions")
    Call <LatestAll> getConditons();

    @GET("conditions/current/")
    Call <LatestAll> getCurrent();

    @GET("relay")
    Call<RelayModel> getRelay();

}