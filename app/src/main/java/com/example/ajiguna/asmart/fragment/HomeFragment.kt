package com.example.ajiguna.asmart.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ajiguna.asmart.R
import com.example.ajiguna.asmart.api.ApiClient
import com.example.ajiguna.asmart.api.ApiInterface
import com.example.ajiguna.asmart.model.Condition
import com.example.ajiguna.asmart.model.LatestAll
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*



/**
 * Created by Aji Guna on 01/03/2018.
 */

class HomeFragment : Fragment() {

    internal lateinit var call_condition: Call<LatestAll>
    private var apiInterface: ApiInterface? = null
    internal var toolbar: Toolbar? = null

    internal var nilai_temp: TextView? = null
    internal var nilai_humi: TextView? = null
    internal var nilai_soil: TextView? = null
    internal var nilai_airp: TextView? = null
    internal var nilai_bat: TextView? = null

    internal var teks_parameter: ArrayList<String> = ArrayList(Arrays.asList( "Kelembaban Udara", "Kelembaban Tanah", "Suhu Udara","Tekanan Udara"))
    internal var img_parameter: ArrayList<Int> = ArrayList<Int>(Arrays.asList(R.drawable.lembab_udara, R.drawable.lembab, R.drawable.suhu, R.drawable.tekanan_udara))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_dashboard, container, false)

        nilai_temp = view.findViewById(R.id.nilai_suhu) as TextView?
        nilai_humi = view.findViewById(R.id.nilai_lembab) as TextView?
        nilai_soil = view.findViewById(R.id.nilai_tanah) as TextView?
        nilai_airp = view.findViewById(R.id.nilai_tekanan) as TextView?
        nilai_bat = view.findViewById(R.id.nilai_bateray) as TextView?

        viewConditions();

        return view;
    }

    private fun viewConditions() {
        apiInterface = ApiClient.getClient().create(ApiInterface::class.java)
        call_condition = apiInterface!!.current

        call_condition.enqueue(object : Callback<LatestAll> {
            override fun onResponse(call_event: Call<LatestAll>, response: Response<LatestAll>) {

                val resource = response.body()
                val kondisi = resource!!.conditions

                for (condition in kondisi) {
                    nilai_temp!!.text = condition.temperature.toString()
                    nilai_humi!!.text = condition.humidity.toString()
                    nilai_soil!!.text = condition.soilmoisture.toString()
                    nilai_airp!!.text = condition.airpressure.toString()
                    nilai_bat!!.text  = condition.batteray.toString()
                }

            }

            override fun onFailure(call_event: Call<LatestAll>, t: Throwable) {
                t.printStackTrace()
            }

        })
    }

    private fun fetchResults(response: Response<LatestAll>): List<Condition> {
        val eventku = response.body()
        return eventku!!.conditions
    }

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

}
