package com.example.ajiguna.asmart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aji Guna on 08/05/2018.
 */

public class LatestAll {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }
}
